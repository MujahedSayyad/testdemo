<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Customer</title>
        <!--<link rel="icon" href="demo_icon.gif" type="image/gif" sizes="16x16">-->
        <style type="text/css">  
            .btn-bs-file{
                position:relative;
            }
            .btn-bs-file input[type="file"]{
                position: absolute;
                top: -9999999;
                filter: alpha(opacity=0);
                opacity: 0;
                width:0;
                height:0;
                outline: none;
                cursor: inherit;
            }
        </style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>

        <form action="index.php/Welcome/importcustomer" method="post">
        <div class="container text-center">
            <div class="row">
                <hr style="margin-bottom:25px;" />
                <div class="col-sm-12" style="margin-bottom:20px;">
                    <label class="btn-bs-file btn btn-lg btn-primary">
                        Browse
                        <input type="file" />
                    </label>

                </div>
            </div>
        </div>
        <input type="submit" value="Upload">
        </form>
    </body>
</html>