<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calculator extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
	        $this->load->helper('func_helper');
	}
        
        public function add(){  
            try{
                $args = func_get_args();
                if(count($args)>0){
                  $arrayVal =  stringToArray($args); 
                  $resultData = arrayValidationAndFiltering($arrayVal);
                  if($resultData['error'] = 1){
                      if(count($resultData['result'])){
                        echo array_sum($resultData['result']);   
                      }else{
                        echo 0;   
                      }                      
                  }else{
                         echo $resultData['error'];  
                  }
                   
                }else{
                  echo 0;    
                }
                
                
            }  catch (Exception $e){
                echo $e;
            }
        }        
        
       public function multiply(){  
            try{
                $args = func_get_args();
                if(count($args)>0){
                  $arrayVal =  stringToArray($args); 
                  $resultData = arrayValidationAndFiltering($arrayVal);
                  if($resultData['error'] = 1){
                      if(count($resultData['result'])){
                        echo array_product($resultData['result']);   
                      }else{
                        echo 0;   
                      }                      
                  }else{
                         echo $resultData['error'];  
                  }
                   
                }else{
                  echo 0;    
                }
                
                
            }  catch (Exception $e){
                echo $e;
            }
        } 
        
        
        
}
